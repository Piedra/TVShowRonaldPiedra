﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVShowENL
{
    public class ESerie
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Language { get; set; }
        public string[] Genres { get; set; }
        public string Status { get; set; }
        public int Runtime { get; set; }
        public string Premiered { get; set; }
        public string OfficialSite { get; set; }
        public Schedule Schedule { get; set; }
        public Rating Rating { get; set; }
        public int Weight { get; set; }
        public Network Network { get; set; }
        public Webchannel WebChannel { get; set; }
        public Externals Externals { get; set; }
        public Image Imagen { get; set; }
        public string Summary { get; set; }
        public int Updated { get; set; }
        public _Links _links { get; set; }
    }
}
