﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TVShowBOL;
using TVShowENL;

namespace TVShowRonaldPiedra
{
    public partial class Form1 : Form
    {
        private SerieBOL sbol;
        private List<ESerie> listaSeries;
        private ESerie actual;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            sbol = new SerieBOL();
            listaSeries = new List<ESerie>();
            actual = new ESerie();
            
        }

        private void btnBusqueda_Click(object sender, EventArgs e)
        {
            WebRequest request = WebRequest.Create("http://api.tvmaze.com/search/shows?q="+txtBusqueda.Text.Trim());
            request.Credentials = CredentialCache.DefaultCredentials;
            WebResponse response = request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);

            string responseFromServer = reader.ReadToEnd();
            reader.Close(); response.Close();
            List<Class1> tvapp = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Class1>>(responseFromServer);
            foreach (Class1 item in tvapp)
            {
                listaSeries.Add(PasarSeries(item));
            }
            dgvSeries.DataSource = listaSeries;
        }

        private ESerie PasarSeries(Class1 item)
        {
            WebClient wc = new WebClient();
            byte[] bytes = new byte[0];
            Image aux = Image.FromFile(@"C:\Users\Fabricio\Downloads\Examen\television.png");
            if (wc.DownloadData(item.show.image.medium) != null)
            {
                bytes = wc.DownloadData(item.show.image.medium);
                MemoryStream ms = new MemoryStream(bytes);
                aux = Image.FromStream(ms);

            }
            ESerie serie = new ESerie
            {
                Id = item.show.id,
                Url = item.show.url,
                Name = item.show.name,
                Type = item.show.type,
                Language = item.show.language,
                Genres = item.show.genres,
                Status = item.show.status,
                Runtime = item.show.runtime,
                Premiered = item.show.premiered,
                OfficialSite = item.show.officialSite,
                Schedule = item.show.schedule,
                Rating = item.show.rating,
                Weight = item.show.weight,
                Network = item.show.network,
                WebChannel = item.show.webChannel,
                Externals = item.show.externals,
                Imagen = aux,
                Summary = item.show.summary,
                Updated = item.show.updated,
                _links = item.show._links
            };
            return serie;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            tbMenuPelis.SelectedIndex = 2;
            dgvFavo.DataSource = sbol.CargarTodos();
        }

        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            tbMenuPelis.SelectedIndex = 1;
            int row = dgvSeries.SelectedRows.Count > 0 ? dgvSeries.SelectedRows[0].Index : -1;
            if (row >= 0)
            {
                actual = dgvSeries.CurrentRow.DataBoundItem as ESerie;
                CargarDatos();
            }

        }

        private void CargarDatos()
        {
            lblNombre.Text = actual.Name;
            lblType.Text = actual.Type;
            lblStat.Text = actual.Status;
            lblTime.Text = actual.Schedule.time;
            lblRating.Text = actual.Rating.average.ToString();
            lblLen.Text = actual.Language;
            lblGenders.Text = string.Join(", ",actual.Genres);
            picMovie.Image = actual.Imagen;
        }

        private void btnAgregarAFav_Click(object sender, EventArgs e)
        {
            tbMenuPelis.SelectedIndex = 0;
            txtBusqueda.Text = ""; 
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (sbol.AgregarAFavoritos(actual))
            {
                Console.WriteLine("Agregada con Exito");
            }

        }
    }
}
