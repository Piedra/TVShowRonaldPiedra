﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVShowDAL;
using TVShowENL;

namespace TVShowBOL
{
    public class SerieBOL
    {
        private SerieDAL sdal;
        public SerieBOL()
        {
            sdal = new SerieDAL();
        }
        public bool AgregarAFavoritos(ESerie actual)
        {
            return sdal.AgregarAFavoritos(actual);
        }

        public List<ESerie> CargarTodos()
        {
            return sdal.CargarTodo();
        }
    }
}
