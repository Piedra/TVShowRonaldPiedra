﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using TVShowENL;

namespace TVShowDAL
{
    public class SerieDAL
    {
        public bool AgregarAFavoritos(ESerie seri)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta

                string sql = @"INSERT INTO public.pelis(
	                            id_usuario, id_serie)
	                            VALUES (1, @id);";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", seri.Externals.thetvdb);
                return cmd.ExecuteNonQuery() > 0;
            }
        }

        public List<ESerie> CargarTodo()
        {
            List<ESerie> usuarios = new List<ESerie>();
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"SELECT id, id_usuario, id_serie
	                         FROM public.pelis WHERE id_usuario = @id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id",1);
                NpgsqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    usuarios.Add(CargarSerie(reader));
                }

            }

            return usuarios;
        }

        private ESerie CargarSerie(NpgsqlDataReader reader)
        {
            string id = reader["id_serie"].ToString();
            return BusquedaPorId(id);
        }

        public ESerie PasarSeries(Class1 item)
        {
            WebClient wc = new WebClient();
            byte[] bytes = new byte[0];
            Image aux = Image.FromFile(@"C:\Users\Fabricio\Downloads\Examen\television.png");
            if (wc.DownloadData(item.show.image.medium) != null)
            {
                bytes = wc.DownloadData(item.show.image.medium);
                MemoryStream ms = new MemoryStream(bytes);
                aux = Image.FromStream(ms);

            }
            ESerie serie = new ESerie
            {
                Id = item.show.id,
                Url = item.show.url,
                Name = item.show.name,
                Type = item.show.type,
                Language = item.show.language,
                Genres = item.show.genres,
                Status = item.show.status,
                Runtime = item.show.runtime,
                Premiered = item.show.premiered,
                OfficialSite = item.show.officialSite,
                Schedule = item.show.schedule,
                Rating = item.show.rating,
                Weight = item.show.weight,
                Network = item.show.network,
                WebChannel = item.show.webChannel,
                Externals = item.show.externals,
                Imagen = aux,
                Summary = item.show.summary,
                Updated = item.show.updated,
                _links = item.show._links
            };
            return serie;
        }



        private ESerie BusquedaPorId(string id)
        {
            WebRequest request = WebRequest.Create("http://api.tvmaze.com/lookup/shows?thetvdb=" + id);
            request.Credentials = CredentialCache.DefaultCredentials;
            WebResponse response = request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);

            string responseFromServer = reader.ReadToEnd();
            reader.Close(); response.Close();
            Class1 tvapp = Newtonsoft.Json.JsonConvert.DeserializeObject<Class1>(responseFromServer);
            return PasarSeries(tvapp);
        }

    }

}
